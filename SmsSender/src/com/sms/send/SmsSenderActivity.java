package com.sms.send;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class SmsSenderActivity extends Activity implements TextWatcher {

	Util util;
	Thread t;

	String site = "http://slidesms.com/solinked/sentsms.php";
	int length = 2;
	EditText code, number, message, editFrequency;
	Button buttonSend, buttonFacebook;
	TextView textSize; 
	ProgressDialog progressDialog;
	int freq = 1;
	int frequency;
	
	
	Facebook facebook = new Facebook("130516427078299");
	AppPreferences appPreferences;

	String completePhoneNumber;
	public static int PICK_CONTACT = 1;
	private static final String TAG = "Sms Sender";
	
	private static final int MAX_MESSAGE_SIZE = 295;
	
	private static final int DIALOG_PROGRESS = 0;
	private static final int DIALOG_ALERT_SUCCESS = 1;
	private static final int DIALOG_ALERT_FAIL = 2;
	private static final int DIALOG_ALERT_CLOSE = 3;
	private static final int LOOP = 5;
	final Handler handler = new Handler() {
		@Override
		public void handleMessage(android.os.Message msg) {
			System.out.println(msg.toString() + "received");
			int x = msg.what;
			
			switch (x) {
			case DIALOG_ALERT_SUCCESS:
				message.setText("");
				showDialog(DIALOG_ALERT_SUCCESS);
				break;
			case DIALOG_ALERT_FAIL:

				showDialog(DIALOG_ALERT_FAIL);
				break;
			case DIALOG_PROGRESS:
				showDialog(DIALOG_PROGRESS);
				break;
			case LOOP:
				progressDialog.setMessage(freq + " message sent");
				break;

			}

		};

	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		util = new Util(this);
		appPreferences = new AppPreferences(this);

		message = (EditText) findViewById(R.id.editTextMessage);
		code = (EditText) findViewById(R.id.editTextCode);
		number = (EditText) findViewById(R.id.editTextNumber);
		textSize = (TextView) findViewById(R.id.textSize);
		editFrequency = (EditText) findViewById(R.id.editNumber);
		buttonFacebook = (Button) findViewById(R.id.buttonFacebook);
		
		buttonFacebook.setText(appPreferences.isLoggedIn()?"Logout":"Login");
		
		
		
		message.addTextChangedListener(this);

	}
	
	
	public void  facebookHandler(View v) {
		
		if(!appPreferences.isLoggedIn()){
			
			facebookAuthentication();
		}else{
			facebookLogout();
		}
		
	}

	public void clickHandler(View v) {

		if(!appPreferences.isLoggedIn()) {
			util.displayToast("Login to facebook first");
			return;
		}
		completePhoneNumber = code.getText().toString()
				+ number.getText().toString();
		
		frequency = Integer.parseInt(editFrequency.getText().toString());
		

		// check internet
		if (!util.isConnected()) {
			util.displayToast("Please Connect to the internet");
			return;
		}

		

		// check message length
		if (message.getText().toString().length() > MAX_MESSAGE_SIZE - appPreferences.getFacebookName().length()) {
			util.displayToast("Message length exceeded 300 characters. Please shorten it !");
			return;
		}

		showDialog(DIALOG_PROGRESS);

		t = new Thread(new Runnable() {

			@Override
			public void run() {

				DefaultHttpClient client = new DefaultHttpClient();
				ResponseHandler<String> res = new BasicResponseHandler();
				HttpPost postMethod = new HttpPost(site);
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						length);

				nameValuePairs.add(new BasicNameValuePair("senderaddress",
						completePhoneNumber));
				String msg = appPreferences.getFacebookName() +"::> "+ message.getText().toString();
				nameValuePairs.add(new BasicNameValuePair("mymessage", msg));
				try {
					postMethod.setEntity(new UrlEncodedFormEntity(
							nameValuePairs));
					for (int i = 0; i < frequency; i++) {
						
						String response =client.execute(postMethod, res);
						Log.d(TAG, response.toString());
						freq = i+1;
						handler.sendEmptyMessage(LOOP);
					}
					dismissDialog(DIALOG_PROGRESS);
					handler.sendEmptyMessage(DIALOG_ALERT_SUCCESS);
				} catch (UnsupportedEncodingException e) {
					dismissDialog(DIALOG_PROGRESS);
					Log.d("2", "here");

					handler.sendEmptyMessage(DIALOG_ALERT_FAIL);
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
					dismissDialog(DIALOG_PROGRESS);
					Log.d("3", "here");

					handler.sendEmptyMessage(DIALOG_ALERT_FAIL);

				} catch (IOException e) {
					e.printStackTrace();
					dismissDialog(DIALOG_PROGRESS);
					Log.d("1", "here");
					handler.sendEmptyMessage(DIALOG_ALERT_FAIL);

				}

			}
		});

		t.start();

	}

	public void contactChooser(View v) {

		Intent intent = new Intent(Intent.ACTION_PICK,
				ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, 1);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		showDialog(DIALOG_ALERT_CLOSE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String phoneNumber = "";
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
		switch (requestCode) {
		case 1:
			if (resultCode == RESULT_OK) {
				Uri contactdata = data.getData();

				Cursor c = managedQuery(contactdata, null, null, null, null);
				if (c.moveToFirst()) {
					String id = c.getString(c
							.getColumnIndex(ContactsContract.Contacts._ID));

					String hasPhone = c
							.getString(c
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
					boolean hasPh = hasPhone.equalsIgnoreCase("1") ? true
							: false;

					if (hasPh) {
						Cursor phone = getContentResolver()
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = " + id, null, null);
						while (phone.moveToNext()) {
							phoneNumber = phone
									.getString(phone
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

						}
					}
				}

			}

			phoneNumber = util.getPhone(phoneNumber);
			if (phoneNumber.length() == 10) {
				code.setText("00977");
			}
			if (phoneNumber.length() > 10) {
				code.setText("");

			}
			number.setText(phoneNumber);
			break;
			
		

		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub

		switch (id) {
		case DIALOG_PROGRESS:
			progressDialog = ProgressDialog.show(this, "Please Wait",
					"Sending Message...");
			/*
			 * p.setCancelable(true); p.setOnCancelListener(new
			 * OnCancelListener() {
			 * 
			 * @Override public void onCancel(DialogInterface dialog) {
			 * 
			 * util.displayToast("cancelled");
			 * 
			 * } });
			 */
			return progressDialog;

		case DIALOG_ALERT_CLOSE:
			AlertDialog.Builder build = new Builder(this);
			build.setMessage("Close the application?");

			build.setCancelable(true);
			build.setPositiveButton("Yes", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

					finish();
				}
			});
			build.setNegativeButton("No", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					dismissDialog(DIALOG_ALERT_CLOSE);
				}
			});
			return build.create();

		case DIALOG_ALERT_FAIL:
			AlertDialog.Builder builder2 = new Builder(this);
			builder2.setTitle("Oops");
			builder2.setMessage("Something went wrong");
			builder2.setCancelable(true);
			builder2.setPositiveButton("Ok", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dismissDialog(DIALOG_ALERT_FAIL);
				}
			});
			return builder2.create();

		case DIALOG_ALERT_SUCCESS:
			AlertDialog.Builder builder3 = new Builder(this);
			builder3.setTitle("Message Sent");
			builder3.setMessage("Send Another?");
			builder3.setCancelable(true);
			builder3.setPositiveButton("Ok", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					dismissDialog(DIALOG_ALERT_SUCCESS);

				}
			});
			builder3.setNegativeButton("Close", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// close the application
					finish();
				}
			});
			return builder3.create();

		default:
			return null;

		}

	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		int  length = message.getText().toString().length();
		int  opposite = MAX_MESSAGE_SIZE-length;
		if(opposite >=20){
			textSize.setTextColor(Color.GREEN);
			textSize.setText(opposite + " characters remaining");

		}else if(opposite <20 && opposite >=10){
			textSize.setTextColor(Color.BLUE);
			textSize.setText(opposite + " characters remaining");

		}else if (opposite>=5 && opposite <10 ){
			textSize.setTextColor(Color.YELLOW);
			textSize.setText(opposite + " characters remaining");

		}else if(opposite <5 && opposite >=0){
			textSize.setTextColor(Color.MAGENTA);

			textSize.setText(opposite + " characters remaining");

		}else{
			textSize.setTextColor(Color.RED);
			textSize.setText(Math.abs(opposite) + " character(s) should be removed");
		}
		
		
		
		
		
		
	}
	public void facebookAuthentication() {
		// TODO Auto-generated method stub
		
		String access_token = appPreferences.getAccessTokenOfFacebook();
		long expires = appPreferences.getAccessExpiresOfFacebook();

		if (access_token != null) {
			facebook.setAccessToken(access_token);
			util.displayToast("already available" + access_token);

		}
		if (expires != 0) {
			facebook.setAccessExpires(expires);
			util.displayToast("already available" + expires);

		}

		if (!facebook.isSessionValid()) {

			Log.d(TAG, "session was not valid");
			facebook.authorize(this, new String[] { "email" },
					new DialogListener() {

						@Override
						public void onComplete(Bundle values) {
							Log.d(TAG, "hrer");
							appPreferences.setAccessTokenOfFacebook(facebook
									.getAccessToken());
							appPreferences.setAccessExpiresOfFacebook(facebook
									.getAccessExpires());
							appPreferences.setLoggedIn(true);
							// appPreferences.setFacebookStatus("logout");

							Log.d(TAG,
									appPreferences.getAccessTokenOfFacebook());

							Log.d(TAG,
									"inside oncomplete of authorize"
											+ appPreferences
													.getAccessExpiresOfFacebook());
							Log.d(TAG, appPreferences.isLoggedIn()+"");
							Log.d(TAG, appPreferences.isLoggedIn() + "");
							Thread t = new Thread(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									final String name = getMyDetails();
									appPreferences.setFacebookName(name);
									Log.d(TAG, "here");
									// set logout message and set name of the
									
									SmsSenderActivity.this.runOnUiThread(new  Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											buttonFacebook.setText("logout");
											
											
										}
									});
									
								
									
								}
							});
							t.start();

						}

						@Override
						public void onFacebookError(FacebookError e) {
							// TODO Auto-generated method stub
							Log.d(TAG, e.toString());
						}

						@Override
						public void onError(DialogError e) {
							// TODO Auto-generated method stub
							Log.d(TAG, e.toString());

						}

						@Override
						public void onCancel() {
							// TODO Auto-generated method stub
							Log.d(TAG, "cancelled");

						}

					});
		}
	}
	public void facebookLogout() {
		// TODO Auto-generated method stub
			final ProgressDialog dialog = ProgressDialog.show(this, "Please Wait", "logging out...");
			Thread t = new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
					
					facebook.logout(SmsSenderActivity.this);
					dialog.dismiss();
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
			});
			t.start();
			
			appPreferences.removeFbDetails();
			Log.d(TAG, "logged out successfully");
			appPreferences.setLoggedIn(false);
			buttonFacebook.setText("Login");

}
	public String getMyDetails() {
		String name = null;
		Bundle b = new Bundle();
		b.putString("access_token", appPreferences.getAccessTokenOfFacebook());
		b.putString("query", "select uid, name from user where uid=me()");
		b.putString("method", "fql.query");
		String response;
		try {
			response = facebook.request(b);

			Log.d(TAG, response.toString());
			JSONArray jsonArray = new JSONArray(response);
			int size = jsonArray.length();
			final String id = jsonArray.getJSONObject(0).getString("uid");
			final String fullname = jsonArray.getJSONObject(0)
					.getString("name");
			name = fullname;
			// JSONObject json = Util.parseJson(response);
			// final String id = json.getString("id");
			// final String fullname = json.getString("name");
			appPreferences.setFacebookId(id);
			appPreferences.setFacebookName(fullname);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Log.d(TAG, "id and name is set");
		return name;

	}
	

	
	
	
}