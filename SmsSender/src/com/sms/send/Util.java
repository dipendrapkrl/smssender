package com.sms.send;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class Util {

	Context context;
	public Util(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	
	
	
/**
 * checks whether internet is available or not
 * @return true if available else false
 */
	public boolean isConnected() {

		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return (netInfo != null && netInfo.isConnectedOrConnecting())?true:false;
		
	}

	
	/**
	 * removes hyphen (-) character and returns
	 * @param x phone no. possibly with hyphen character
	 * @return phone number without hyphen character
	 */
	public String getPhone(String x) {
		
		String y ="";
		for (int i = 0; i < x.length(); i++) {
			if(x.charAt(i) =='-')continue;
			y = y+x.charAt(i);
		}
		
		return y;
	}
	
	public void displayToast(String message) {
		
		new Toast(context);
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	
	public String getPhoneNumber() {
		
		TelephonyManager tm = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
		String  x = tm.getSubscriberId();
		
		Log.d("this", x);
		Log.d("this", tm.getLine1Number());
		
		if(tm.getLine1Number() == null){
			return " ";
		}else{
			return tm.getLine1Number().substring(2);
		}
		
	}
}
